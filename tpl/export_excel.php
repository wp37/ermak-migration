<?php
	//��� ������ ���������� ������ PHP 5.2.0 ��� ����. � ����� ���������� ��������� ����������: php_zip, php_xml � php_gd2
	
	// ���������� ����� ��� ������ � excel
	require_once(ERMAK_MIGRATION_REAL_PATH . 'PHPExcel_1.8.0/Classes/PHPExcel.php');
	// ���������� ����� ��� ������ ������ � ������� excel
	require_once(ERMAK_MIGRATION_REAL_PATH . 'PHPExcel_1.8.0/Classes/PHPExcel/Writer/Excel5.php');
	 
	// ������� ������ ������ PHPExcel
	$xls = new PHPExcel();
	// ������������� ������ ��������� �����
	$xls->setActiveSheetIndex(0);
	// �������� �������� ����
	$sheet = $xls->getActiveSheet();
	// ����������� ����
	$sheet->setTitle('������� ���������');
	 
	// ��������� ����� � ������ A1
	$sheet->setCellValue("A1", '������� ���������');
	$sheet->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');
	 
	// ���������� ������
	$sheet->mergeCells('A1:H1');
	 
	// ������������ ������
	$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	 
	for ($i = 2; $i < 10; $i++) 
	{
		for ($j = 2; $j < 10; $j++) 
		{
			// ������� ������� ���������
			$sheet->setCellValueByColumnAndRow(
												  $i - 2,
												  $j,
												  $i . "x" .$j . "=" . ($i*$j)
											  );
			// ��������� ������������
			$sheet->getStyleByColumnAndRow($i - 2, $j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
	}
	
	// ������� HTTP-���������
	/*
	header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
	header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
	header ( "Cache-Control: no-cache, must-revalidate" );
	header ( "Pragma: no-cache" );
	header ( "Content-type: application/vnd.ms-excel" );
	header ( "Content-Disposition: attachment; filename=matrix.xls" );
	*/
	 
	// ������� ���������� �����
	 $objWriter = new PHPExcel_Writer_Excel5($xls);
	 $objWriter->save('php://output');
?>