<?php
	global $result_html, $new_dir;
	
	//foreach($arr as $type)
	{
		global $Soling_Metagame_Constructor, $taxonomy_images_plugin;
		require_once(SMC_REAL_PATH."tpl/post_trumbnail.php");
		if(!$name)	$name=date_i18n( '_m_j_Y_G_i' );
		$new_dir	= ERMAK_MIGRATION_REAL_PATH."temp/" . $name;
		if(!@mkdir($new_dir, 0777))
		{
			@clear_dir($new_dir);
			if(!@mkdir($new_dir, 0777))
			{
				return 'Error!';
			}
		}			
		$result_html		= "<div style='width:500px;'>";
		require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
		$SMC_Object_type	= SMC_Object_type::get_instance();
		$keys				= array_keys($SMC_Object_type->object);
		$values				= array_values($SMC_Object_type->object);
		$all				= array();
		$result_html		.= 1;
		for($i=0; $i<count($keys); $i++)
		{	
			if(count(array_intersect($arr, array($keys[$i])))==0  && $SMC_Object_type->get_type($keys[$i]) != "option") continue;
			switch( $SMC_Object_type->get_type($keys[$i]))
			{
			/**/
				case "post":
				//case "page":
					$args		= array(
										"numberposts"		=> -1,
										"offset"			=> 0,
										'orderby'  			=> 'title',
										'order'     		=> 'ASC',
										'fields'			=> 'ids',
										'post_type' 		=> $keys[$i],
										'post_status' 		=> 'publish',									
									);
				
					$posts		= get_posts($args);
					$result_html		.= "<h3>".$keys[$i]."</h3>";
					//$all[$keys[$i]]	= array();
					foreach($posts as $post)
					{
						$elem			= $SMC_Object_type->get_object($post, $keys[$i]);
						$result_html	.= "<div class=abzaz>" . json_encode($elem, JSON_UNESCAPED_UNICODE ). "</div>";
						$all[]	= $elem;
					}
					break;	
				
				case "taxonomy":
					
					$args		= array(
										'number' 		=> 120
										,'offset' 		=> 0
										,'orderby' 		=> 'id'
										,'order' 		=> 'ASC'
										,'hide_empty' 	=> false
										,'fields' 		=> 'all'
										,'slug' 		=> ''
										,'hierarchical' => true
										,'name__like' 	=> ''
										,'pad_counts' 	=> false
										,'get' 			=> ''
									);
							
					$childLocs			= get_terms($keys[$i], $args);
					
					$result_html		.= "<h3>".$keys[$i]."</h3>";
					//$all[$keys[$i]]		= array();
					foreach($childLocs as $post)
					{
						$elem	= $SMC_Object_type->get_object($post->term_id, $keys[$i]);
						$result_html	.= "<div class=abzaz>" . json_encode($elem, JSON_UNESCAPED_UNICODE). "</div>";
						$all[]	= $elem;
					}
					break;
				
				case "option":
					$result_html		.= "<h4>option: ".$keys[$i]."</h4>";
					$result_html		.= "<div class=abzaz>" .   Assistants::echo_me($values[$i]['data'], true). "</div>";
					$all[$keys[$i]]		= $values[$i];
					break;
				default:
					$result_html		.= "<h4>".$keys[$i]."</h4>";
					$result_html		.= "<div class=abzaz>" . $values[$i]. "</div>";
					$all[]				= $values[$i];
				/**/
			}
		}
	}	
		$result_html		.=  2;
		$fp = fopen($new_dir."/data.json","wb");
		fwrite($fp,json_encode($all));//, JSON_UNESCAPED_UNICODE));
		fclose($fp);
		
		$result_html = "<div style='width:500px;'>".Assistants::echo_me($all, true)."</div>";
		//$result_html		.= "</div>";
?>