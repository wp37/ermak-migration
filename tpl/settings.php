<?php
	function get_name_by_type($type, $arrays)
	{
		foreach($arrays as $array)
		{
			if($array->name == $type)	
				return $array->label;
		}
		return $type;
	}	
	require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
	$SMC_Object_type	= SMC_Object_type::get_instance();
	$keys				= array_keys($SMC_Object_type->object);
	$values				= array_values($SMC_Object_type->object);
	
	$post_types			= get_post_types('','object');
	$taxonomies			= get_taxonomies(array(), "object");
	$names				= array_merge($post_types, $taxonomies);
	
	if($_POST['proba'])
	{
		$elem			= (get_one_scenario_element("minimum", "goods_type", "myaso"));
		$d				= $SMC_Object_type->get("goods_type");
		$components		= $SMC_Object_type->convert_id("components", $elem['components2'], $d, $gt_id);
		echo "<div class='updated notice smc_notice_ is-dismissible1'><h2>".$elem['title'] ."</h2><div>". Assistants::echo_me($components, true). Assistants::echo_me($elem['components2'], true)."</div></div>";
		/*
		global $GoodsType;
		$gt_id			= 83;
		$d				= $SMC_Object_type->get("goods_type");
		$co				= $GoodsType->get_components($gt_id);
		$components		= $SMC_Object_type->convert_id("components", $co, $d, $gt_id);
		echo "<div class='updated notice smc_notice_ is-dismissible1'>".Goods_Type::get_trumbnail($gt_id) ."<div>". Assistants::echo_me($components, true). Assistants::echo_me($co, true)."</div></div>";
		*/
	}
	
	$htm				= "
	<h1>".__("Ermak. Migration", "ermak_migration"). "</h1>
	<form name='settings'  method='post'  enctype='multipart/form-data' id='settings'>";
	$html				= "
		<div class='smc_form'>
			<board_title>".__("Object types", "ermak_migration")."</board_title>
			<div id='isend'>
			
			</div>".$hh;
	for($i=0; $i<count($keys); $i++)
	{
		if( $SMC_Object_type->get_type($keys[$i]) == "option" ) continue;
		$checked		= 
			!( $keys[$i] == "smp_currency_account" || $keys[$i] == "direct_message" )
			? "checked" : "";
		$name			= get_name_by_type($keys[$i], $names);
		$html			.= "
		<div>
			<input type='checkbox' class='css-checkbox' name='types$i' id='types$i' value='".$keys[$i]."' $checked /> 
			<label class='css-label' for='types$i'>".$name."</label>
		</div>";
	}
	
	if ($handle = opendir(ERMAK_MIGRATION_REAL_PATH."temp")) 
	{
		//if ($handle = opendir('.')) 
		{
			while (false !== ($file = readdir($handle))) 
			{ 
				if ($file != "." && $file != "..") 
				{ 
					$txt	= sprintf( __("Are you realy want remove all Ermak datas and install scenario %s?", "ermak_migration."), "<BR><b>$file</b>" );
					$yes	= __("Yes");
					$ht 	.= "
					<div style='margin:2px 0;'>
						<div class='button' scenarion_id='$file' style='height: 125px; display: block; position: relative; padding: 10px;' text='$txt' yes='$yes'>
						<table style='display:block; position:relative;'>
							<tr>
								<td rowspan='4'>
									<img src='".ERMAK_MIGRATION_URLPATH."img/ermak_logo.jpg'/>
								</td>
								<td>" . 
									" $file 
								</rd>
							</tr>
						</table>
						</div>
					</div>"; 
				} 
			}
			closedir($handle); 
		}
	}
	
	$html				.= "
			<div style='margin-top:10px;'>					
				<div id='download_scenario' class='button' text='".__("Choose name of project and pictogramm", "ermak_migration")."' accsess='".__("Start","ermak_migration")."'>" . __("Download JSON-file", "ermak_migration") . "</div>
				
				<div id='download_excel' class='button' >" . __("Download Excel", "ermak_migration") . "</div>				
				
				<div class='lp-hide' id='new_sc_title' >
					<input type='text' value=' ' class='inpname'/>					
				</div>
				<input type='hidden'  name='n' value='$i'/>
			</div>
		</div>";
	$html2_				.= "
		<div class='smc_form'>
			<board_title>".__("Proba", "ermak_migration")."</board_title>
			<input type='submit' name='proba' value='" . __("Click me", "ermak_migration") . "' />
		</div>";
	$html2				.= "
		<div class='smc_form'>
			<board_title>".__("New Scenario", "ermak_migration")."</board_title>
			$ht 
			<!--input type='submit' name='install_scenario' value='" . __("Install Scenario", "ermak_migration") . "' /-->
		</div>";
	
	$htm1		= "</form>";
	echo $htm. Assistants::get_switcher(
		array(
			array("title"=>__("Save Scenario","ermak_migration"), "slide"=>$html),
			array("title"=>__("Install Scenario","ermak_migration"), "slide"=>$html2)
		), 
		"er_mig"
		).
	$htm1;
	
	
	function get_one_scenario_element($scenario_file, $element_type, $element_slug)
	{		
		require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
		$SMC_Object_type= SMC_Object_type::get_instance();
		define('ERMAK_MIGRATION_PATH', ERMAK_MIGRATION_REAL_PATH."temp/".$scenario_file."/");
		$migration_url 	= ERMAK_MIGRATION_URLPATH."temp/".$scenario_file."/";
		$contents 		= file_get_contents(ERMAK_MIGRATION_REAL_PATH."temp/".$scenario_file."/data.json");
		$data			= json_decode($contents, true);
		$d				=  $SMC_Object_type->get($element_type);
		foreach($data as $dat)
		{
			if($dat['post_type'] == $element_type && $dat['name'] == $element_slug)
			{
				return $dat;
			}
		}
		return "NONE!!!!!";
	}
?>